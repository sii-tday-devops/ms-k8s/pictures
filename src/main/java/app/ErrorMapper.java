package app;

import javax.json.Json;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.http.HttpServerRequest;

@Provider
public class ErrorMapper implements ExceptionMapper<Exception> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorMapper.class);

    @Context
    UriInfo info;

    @Context
    HttpServerRequest request;

    @Override
    public Response toResponse(Exception exception) {
        int code = 500;

        if (exception instanceof WebApplicationException) {
            code = ((WebApplicationException) exception).getResponse().getStatus();
        }

        final String method = request.rawMethod();
        final String path = request.path();

        LOGGER.error("Request {} {} {} {}", method, path, code, exception.getMessage(), exception);

        return Response.status(code)
                .entity(
                        Json.createObjectBuilder()
                                .add("error", exception.getMessage())
                                .add("code", code)
                                .build()
                )
                .build();
    }

}
